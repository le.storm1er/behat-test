FROM composer:2 as composer

FROM php:7

COPY --from=composer /usr/bin/composer /usr/bin/composer

RUN apt -y update && apt -y install libzip-dev gnupg gnupg2
RUN docker-php-ext-install zip
RUN docker-php-ext-enable zip
RUN curl -sS -o - https://dl-ssl.google.com/linux/linux_signing_key.pub | apt-key add 
RUN bash -c "echo 'deb [arch=amd64] http://dl.google.com/linux/chrome/deb/ stable main' >> /etc/apt/sources.list.d/google-chrome.list" 
RUN apt -y update && apt -y install google-chrome-stable

ENV PANTHER_CHROME_ARGUMENTS "--disable-dev-shm-usage --disable-extensions --no-sandbox --whitelisted-ips=''"


