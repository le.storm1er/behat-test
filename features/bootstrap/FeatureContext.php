<?php

use Behat\Behat\Context\Context;
use Behat\Gherkin\Node\PyStringNode;
use Behat\Gherkin\Node\TableNode;
use Behat\MinkExtension\Context\MinkContext;

/**
 * Defines application features from the specific context.
 */
class FeatureContext extends MinkContext
{
    /**
     * Initializes context.
     *
     * Every scenario gets its own context instance.
     * You can also pass arbitrary arguments to the
     * context constructor through behat.yml.
     */
    public function __construct()
    {        
        // $this->page = $this->getSession()->getPage();

    }

    /**
     * @When wait for the element ":selector" to be loaded
     */
    public function waitForTheElementToBeLoaded($selector)
    {
        $this->getSession()->wait(5000, 'document.querySelector("'.$selector.'")');
    }

    /**
     * @Given I am logged in ":url"
     */
    public function iAmLoginIn($url)
    {
        $this->visit($url);
        $this->waitForTheElementToBeLoaded(".btn-action-quable");
        $this->fillField("Email or username", "quable");
        $this->fillField("Please enter your password", "Quable*2018€");
        $this->clickOnElement(".btn-action-quable");
        $this->waitForTheElementToBeLoaded("input[role='combobox']");
        
    }


    /**
     * @When I click on element :selector
     */
    public function clickOnElement($selector) {        
        $page = $this->getSession()->getPage();

        $element = $page->find('css', '.btn-action-quable');

        // if (null === $element) {
        //     throw new ElementNotFoundException($this->getDriver(), 'element', 'css selector', $selector);
        // }

        $element->click();
    }

    /**
     * @Then I should have an element with '(?P<text>(?:[^"]|\\")*)' as content
     */
    public function ShouldHaveElementWithClass($text) {
        $page = $this->getSession()->getPage();

        $element = $page->find('content', $this->fixStepArgument($text));


    }
}
