Feature: Logging

  @javascript
  Scenario: Accessing dev-sebastien
    Given I am on "https://dev-sebastien.quable.com"
    When wait for the element ".btn-action-quable" to be loaded
    And I fill in "Email or username" with "quable"
    And I fill in "Please enter your password" with "Quable*2018€"
    And I click on element ".btn-action-quable"
    And wait for the element "input[role='combobox']" to be loaded
    Then I should see "Hello support"