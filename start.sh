#!/bin/sh

set -e

docker build . -t test:behat

docker run -it --rm --user `id -u`:`id -g` -v `pwd`:/app --workdir /app test:behat composer install
docker run -it --rm --user `id -u`:`id -g` -v `pwd`:/app --workdir /app test:behat bash

